(function() {
  var Backbone, Chaos, _;

  window.Chaos = {
    Collections: {},
    Models: {},
    Views: {},
    Routers: {},
    init: function() {
      this.nav_view = new Chaos.Views.Nav();
      this.search_view = new Chaos.Views.Search();
      this.cart = new Chaos.Models.Cart();
      this.cart_view = new Chaos.Views.Cart({
        model: this.cart
      });
      this.products_views = [];
      $(".js-products").each((function(_this) {
        return function(index, el) {
          return _this.products_views.push(new Chaos.Views.Products({
            el: $(el)
          }));
        };
      })(this));
      this.product_views = [];
      $(".js-product").each((function(_this) {
        return function(index, el) {
          return _this.product_views.push(new Chaos.Views.Product({
            el: $(el)
          }));
        };
      })(this));
      this.sort_views = [];
      $(".js-sort").each((function(_this) {
        return function(index, el) {
          return _this.sort_views.push(new Chaos.Views.Sort({
            el: $(el)
          }));
        };
      })(this));
      this.breadcrumbs_views = [];
      $(".js-breadcrumbs").each((function(_this) {
        return function(index, el) {
          return _this.breadcrumbs_views.push(new Chaos.Views.Breadcrumbs({
            el: $(el)
          }));
        };
      })(this));
      this.collections_views = [];
      $(".js-collections").each((function(_this) {
        return function(index, el) {
          return _this.collections_views.push(new Chaos.Views.Collections({
            el: $(el)
          }));
        };
      })(this));
      this.login_views = [];
      return $(".js-login").each((function(_this) {
        return function(index, el) {
          return _this.login_views.push(new Chaos.Views.Login({
            el: $(el)
          }));
        };
      })(this));
    }
  };

  Chaos = window.Chaos;

  _ = window._;

  Backbone = window.Backbone;

  $(function() {
    return Chaos.init();
  });

}).call(this);

(function() {
  Handlebars.registerHelper('money', function(value) {
    if (value != null) {
      return "<span class=money>$" + (parseFloat(value) / 100).toFixed(2) + "</span>";
    } else {
      return null;
    }
  });

  Handlebars.registerHelper('truncate', function(string, end) {
    if (string != null) {
      string = string.slice(0, end);
      if (string.length >= end) {
        string += "...";
      }
      return string;
    } else {
      return null;
    }
  });

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Models.Cart = (function(superClass) {
    extend(Cart, superClass);

    function Cart() {
      return Cart.__super__.constructor.apply(this, arguments);
    }

    Cart.prototype.url = "/cart.js";

    Cart.prototype.parse = function(response) {
      return response;
    };

    Cart.prototype.initialize = function() {
      return this.fetch();
    };

    Cart.prototype.add = function(id, quantity) {
      if (quantity == null) {
        quantity = 1;
      }
      return $.ajax("/cart/add.js", {
        method: "POST",
        dataType: "json",
        data: {
          quantity: quantity,
          id: parseInt(id)
        },
        success: (function(_this) {
          return function(response) {
            return _this.fetch();
          };
        })(this)
      });
    };

    Cart.prototype.change = function(id, quantity) {
      var post;
      console.log(id);
      console.log(quantity);
      return post = $.ajax("/cart/change.js", {
        method: "POST",
        dataType: "json",
        data: {
          quantity: quantity,
          id: parseInt(id)
        },
        success: (function(_this) {
          return function(response) {
            console.log(response);
            _this.set(response);
            return _this.trigger("sync");
          };
        })(this)
      });
    };

    Cart.prototype.remove = function(id) {
      return this.change(id, 0);
    };

    return Cart;

  })(Backbone.Model);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Breadcrumbs = (function(superClass) {
    extend(Breadcrumbs, superClass);

    function Breadcrumbs() {
      return Breadcrumbs.__super__.constructor.apply(this, arguments);
    }

    Breadcrumbs.prototype.events = {};

    Breadcrumbs.prototype.initialize = function() {
      return this.render();
    };

    Breadcrumbs.prototype.render = function() {
      var snap;
      snap = $(".js-breadcrumbs_snap");
      if (snap.length > 0) {
        this.top = snap.position().top;
        this.$el.css("top", this.top);
        $(window).off("scroll");
        $(window).scroll((function(_this) {
          return function() {
            if (window.pageYOffset > _this.top - 75) {
              if (!_this.$el.hasClass("breadcrumbs--fixed")) {
                return _this.$el.addClass("breadcrumbs--fixed");
              }
            } else {
              if (_this.$el.hasClass("breadcrumbs--fixed")) {
                return _this.$el.removeClass("breadcrumbs--fixed");
              }
            }
          };
        })(this));
      }
      return this;
    };

    return Breadcrumbs;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Cart = (function(superClass) {
    extend(Cart, superClass);

    function Cart() {
      return Cart.__super__.constructor.apply(this, arguments);
    }

    Cart.prototype.el = $("#cart");

    Cart.prototype.template = templates["cart"];

    Cart.prototype.data = {};

    Cart.prototype.events = {
      "click .js-cart__button": "toggle_cart",
      "click .js-remove_from_cart": "remove_from_cart",
      "click .js-increment": "increment",
      "click .js-decrement": "decrement"
    };

    Cart.prototype.initialize = function() {
      this.listenTo(this.model, "sync", this.render);
      return this.render();
    };

    Cart.prototype.render = function() {
      _.extend(this.data, {
        model: this.model.toJSON()
      });
      this.$el.html(this.template(this.data));
      if (this.model.get("item_count") === 0) {
        this.hide_cart();
      }
      Currency.convertAll(shopCurrency, cookieCurrency);
      return this;
    };

    Cart.prototype.toggle_cart = function(e) {
      e.preventDefault();
      return this.$el.toggleClass("cart--opened");
    };

    Cart.prototype.show_cart = function() {
      return this.$el.addClass("cart--opened");
    };

    Cart.prototype.hide_cart = function() {
      return this.$el.removeClass("cart--opened");
    };

    Cart.prototype.remove_from_cart = function(e) {
      e.preventDefault();
      return Chaos.cart.remove($(e.currentTarget).attr("data-id"));
    };

    Cart.prototype.increment = function(e) {
      e.preventDefault();
      return Chaos.cart.change($(e.currentTarget).attr("data-id"), parseInt($(e.currentTarget).attr("data-current-quantity")) + 1);
    };

    Cart.prototype.decrement = function(e) {
      e.preventDefault();
      return Chaos.cart.change($(e.currentTarget).attr("data-id"), parseInt($(e.currentTarget).attr("data-current-quantity")) - 1);
    };

    return Cart;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Collections = (function(superClass) {
    extend(Collections, superClass);

    function Collections() {
      return Collections.__super__.constructor.apply(this, arguments);
    }

    Collections.prototype.events = {};

    Collections.prototype.initialize = function() {
      return this.render();
    };

    Collections.prototype.render = function() {
      var collections;
      collections = this.$el.find(".js-collection.hide");
      if (collections.length > 0) {
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
        collections = this.$el.find(".js-collection.hide");
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
        collections = this.$el.find(".js-collection.hide");
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
      } else {
        collections = this.$el.find(".js-product").addClass("hide");
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
        collections = this.$el.find(".js-product.hide");
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
        collections = this.$el.find(".js-product.hide");
        $(collections[Math.floor(Math.random() * collections.length)]).removeClass("hide");
      }
      return this;
    };

    return Collections;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Login = (function(superClass) {
    extend(Login, superClass);

    function Login() {
      return Login.__super__.constructor.apply(this, arguments);
    }

    Login.prototype.events = {
      "click .js-recover_button": "show_recover",
      "click .js-login_button": "show_login"
    };

    Login.prototype.initialize = function() {
      return this.render();
    };

    Login.prototype.render = function() {
      return this;
    };

    Login.prototype.show_recover = function(e) {
      e.preventDefault();
      this.$el.find(".js-recover_form").removeClass("hide");
      return this.$el.find(".js-login_form").addClass("hide");
    };

    Login.prototype.show_login = function(e) {
      e.preventDefault();
      this.$el.find(".js-login_form").removeClass("hide");
      return this.$el.find(".js-recover_form").addClass("hide");
    };

    return Login;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Nav = (function(superClass) {
    extend(Nav, superClass);

    function Nav() {
      return Nav.__super__.constructor.apply(this, arguments);
    }

    Nav.prototype.el = $("#nav");

    Nav.prototype.events = {
      "touchend .js-nav__button": "tap_show_container",
      "mouseenter .js-nav__button": "slow_show_container",
      "mouseleave .js-nav__super_container": "hide_container",
      "mouseenter .js-nav__container": "clear_timeout",
      "mouseleave .js-nav__container": "hide_container",
      "click .js-search_button": "toggle_search"
    };

    Nav.prototype.initialize = function() {
      return this.render();
    };

    Nav.prototype.render = function() {
      return this;
    };

    Nav.prototype.tap_show_container = function(e) {
      e.preventDefault();
      if ($(e.currentTarget).hasClass("button--focused")) {
        return this.hide_container(e);
      } else {
        return this.show_container(e);
      }
    };

    Nav.prototype.slow_show_container = function(e) {
      if (this.$el.find(".nav__container--active").length > 0) {
        if (this.show_timeout != null) {
          clearTimeout(this.show_timeout);
        }
        return this.show_timeout = setTimeout((function(_this) {
          return function() {
            return _this.show_container(e);
          };
        })(this), 200);
      } else {
        return this.show_container(e);
      }
    };

    Nav.prototype.clear_timeout = function(e) {
      if (this.show_timeout != null) {
        return clearTimeout(this.show_timeout);
      }
    };

    Nav.prototype.show_container = function(e) {
      var button;
      button = $(e.currentTarget);
      this.$el.find(".js-nav__container").removeClass("nav__container--active");
      this.$el.find(".js-nav__button").removeClass("button--focused");
      button.addClass("button--focused");
      if (button[0].hasAttribute("href")) {
        return button.next().next().addClass("nav__container--active");
      } else {
        return button.next().addClass("nav__container--active");
      }
    };

    Nav.prototype.hide_container = function(e) {
      this.$el.find(".js-nav__container").removeClass("nav__container--active");
      return this.$el.find(".js-nav__button").removeClass("button--focused");
    };

    Nav.prototype.toggle_search = function(e) {
      return Chaos.search_view.toggle_search(e);
    };

    return Nav;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Product = (function(superClass) {
    extend(Product, superClass);

    function Product() {
      return Product.__super__.constructor.apply(this, arguments);
    }

    Product.prototype.events = {
      "submit .js-add_to_cart": "add_to_cart",
      "click [name='color']": "change_color",
      "click [name='size']": "change_size",
      "click .js-extra_image": "load_extra_image"
    };

    Product.prototype.initialize = function() {
      var current;
      current = this.$el.find("[name='id']");
      this.current_color = current.attr("data-color");
      this.current_size = current.attr("data-size");
      return this.render();
    };

    Product.prototype.render = function() {
      _.each(variant_colors, (function(_this) {
        return function(color, key) {
          return _this.$el.find("[data-color='" + key + "'] + label").css("background", color);
        };
      })(this));
      this.$el.find("[data-color~='White']").addClass("radio--variant--bordered");
      this.$el.find("[data-color~='Natural']").addClass("radio--variant--bordered");
      return this;
    };

    Product.prototype.add_to_cart = function(e) {
      var el;
      e.preventDefault();
      el = this.$el.find("[name='id']");
      if (el.prop("type") === "hidden") {
        Chaos.cart.add(this.$el.find("[name='id'][checked='checked']").val());
      } else {
        Chaos.cart.add(this.$el.find("[name='id']:checked").val());
      }
      return Chaos.cart_view.show_cart();
    };

    Product.prototype.change_color = function(e) {
      this.$el.find(".js-image").attr("src", $(e.currentTarget).attr("data-img"));
      this.$el.find(".js-link").attr("href", $(e.currentTarget).attr("data-url"));
      this.current_color = $(e.currentTarget).attr("data-color");
      return this.update_id();
    };

    Product.prototype.change_size = function(e) {
      this.current_size = $(e.currentTarget).attr("data-size");
      return this.update_id();
    };

    Product.prototype.update_id = function() {
      var id;
      this.$el.find("[name='id']").removeAttr("checked");
      if ((this.current_size != null) && this.current_size !== "") {
        id = this.$el.find("[name='id'][data-color='" + this.current_color + "'][data-size='" + this.current_size + "']");
      } else {
        id = this.$el.find("[name='id'][data-color='" + this.current_color + "']");
      }
      if (id[0].hasAttribute("disabled")) {
        return this.$el.find(".js-add_to_cart_button").addClass("button--sold_out");
      } else {
        this.$el.find(".js-add_to_cart_button").removeClass("button--sold_out");
        return id.attr("checked", "checked");
      }
    };

    Product.prototype.load_extra_image = function(e) {
      e.preventDefault();
      return this.$el.find(".js-image").attr("src", $(e.currentTarget).attr("data-url"));
    };

    return Product;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Products = (function(superClass) {
    extend(Products, superClass);

    function Products() {
      return Products.__super__.constructor.apply(this, arguments);
    }

    Products.prototype.events = {
      "click .js-next": "load_more",
      "click .js-search_button": "toggle_search"
    };

    Products.prototype.initialize = function() {
      return this.render();
    };

    Products.prototype.render = function() {
      var next;
      next = this.$el.find(".js-next");
      return this;
    };

    Products.prototype.load_more = function(e) {
      var next;
      if (e != null) {
        e.preventDefault();
      }
      next = this.$el.find(".js-next");
      return $.ajax(next.attr("href"), {
        method: "GET",
        dataType: "html",
        success: (function(_this) {
          return function(response) {
            var response_next;
            _this.$el.find(".js-products_container").append($(response).find(".js-products_container").html());
            response_next = $(response).find(".js-next");
            if (response_next.length > 0) {
              next.attr("href", response_next.attr("href"));
            } else {
              next.remove();
            }
            _this.render();
            next.blur();
            _.each(Chaos.product_views, function(view) {
              return view.undelegateEvents();
            });
            Chaos.product_views = [];
            return $(".js-product").each(function(index, el) {
              return Chaos.product_views.push(new Chaos.Views.Product({
                el: $(el)
              }));
            });
          };
        })(this)
      });
    };

    Products.prototype.toggle_search = function(e) {
      return Chaos.search_view.toggle_search(e);
    };

    return Products;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Search = (function(superClass) {
    extend(Search, superClass);

    function Search() {
      return Search.__super__.constructor.apply(this, arguments);
    }

    Search.prototype.el = $("#search");

    Search.prototype.events = {
      "click .js-hide_search": "hide_search"
    };

    Search.prototype.initialize = function() {
      return this.render();
    };

    Search.prototype.render = function() {
      return this;
    };

    Search.prototype.toggle_search = function(e) {
      if (e != null) {
        e.preventDefault();
      }
      this.$el.toggleClass("fade_in");
      if (this.$el.hasClass("fade_in")) {
        return this.$el.find("[name='q']").first().focus();
      }
    };

    Search.prototype.show_search = function(e) {
      if (e != null) {
        e.preventDefault();
      }
      this.$el.addClass("fade_in");
      return this.$el.find("[name='q']").first().focus();
    };

    Search.prototype.hide_search = function(e) {
      if (e != null) {
        e.preventDefault();
      }
      return this.$el.removeClass("fade_in");
    };

    return Search;

  })(Backbone.View);

}).call(this);

(function() {
  var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Chaos.Views.Sort = (function(superClass) {
    extend(Sort, superClass);

    function Sort() {
      return Sort.__super__.constructor.apply(this, arguments);
    }

    Sort.prototype.events = {
      "change [name='sort']": "change_sort",
      "change [name='tag']": "change_tag",
      "change [name='type']": "change_type"
    };

    Sort.prototype.initialize = function() {
      return this.render();
    };

    Sort.prototype.render = function() {
      return this;
    };

    Sort.prototype.change_sort = function(e) {
      return location.search = "sort_by=" + e.currentTarget.value;
    };

    Sort.prototype.change_tag = function(e) {
      return location.pathname = e.currentTarget.value;
    };

    Sort.prototype.change_type = function(e) {
      return location.pathname = e.currentTarget.value;
    };

    return Sort;

  })(Backbone.View);

}).call(this);
