window.Chaos =
	Collections:{}
	Models:{}
	Views:{}
	Routers:{}


	init: ->

		
		@nav_view = new Chaos.Views.Nav()
		@search_view = new Chaos.Views.Search()

		@cart = new Chaos.Models.Cart()
		@cart_view = new Chaos.Views.Cart
			model: @cart



		@products_views = []
		$(".js-products").each (index, el)=>
			@products_views.push new Chaos.Views.Products({el: $(el)})



		@product_views = []
		$(".js-product").each (index, el)=>
			@product_views.push new Chaos.Views.Product({el: $(el)})



		@sort_views = []
		$(".js-sort").each (index, el)=>
			@sort_views.push new Chaos.Views.Sort({el: $(el)})


		@breadcrumbs_views = []
		$(".js-breadcrumbs").each (index, el)=>
			@breadcrumbs_views.push new Chaos.Views.Breadcrumbs({el: $(el)})


		@collections_views = []
		$(".js-collections").each (index, el)=>
			@collections_views.push new Chaos.Views.Collections({el: $(el)})



		@login_views = []
		$(".js-login").each (index, el)=>
			@login_views.push new Chaos.Views.Login({el: $(el)})




		


		# 	Backbone.history.start
		# 		pushState: true





Chaos = window.Chaos
_ = window._
Backbone = window.Backbone






$ ->

	Chaos.init()



