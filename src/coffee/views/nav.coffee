class Chaos.Views.Nav extends Backbone.View

	el: $("#nav")


	events: 
		"touchend .js-nav__button": "tap_show_container"
		"mouseenter .js-nav__button": "slow_show_container"
		"mouseleave .js-nav__super_container": "hide_container"
		"mouseenter .js-nav__container": "clear_timeout"
		"mouseleave .js-nav__container": "hide_container"
		"click .js-search_button": "toggle_search"
	




	initialize: ->

		this.render()

		



	render: ->

		this



	tap_show_container: (e)->
		e.preventDefault()

		if $(e.currentTarget).hasClass "button--focused"
			this.hide_container(e)

		else
			this.show_container(e)
			


	slow_show_container: (e)->
		if this.$el.find(".nav__container--active").length > 0
			clearTimeout @show_timeout if @show_timeout?
			@show_timeout = setTimeout =>
				this.show_container(e)

			, 200

		else
			this.show_container(e)


	clear_timeout: (e)->
		clearTimeout @show_timeout if @show_timeout?



	show_container: (e)->
		button = $(e.currentTarget)

		this.$el.find(".js-nav__container").removeClass "nav__container--active"
		this.$el.find(".js-nav__button").removeClass "button--focused"

		button.addClass "button--focused"

		if button[0].hasAttribute("href")
			button.next().next().addClass "nav__container--active"

		else
			button.next().addClass "nav__container--active"



	hide_container: (e)->
		this.$el.find(".js-nav__container").removeClass "nav__container--active"
		this.$el.find(".js-nav__button").removeClass "button--focused"


	toggle_search: (e)->
		Chaos.search_view.toggle_search e







