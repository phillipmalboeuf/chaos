class Chaos.Views.Search extends Backbone.View

	el: $("#search")


	events: 
		"click .js-hide_search": "hide_search"
	




	initialize: ->

		this.render()

		



	render: ->

		this



	toggle_search: (e)->
		e.preventDefault() if e?

		this.$el.toggleClass "fade_in"

		if this.$el.hasClass "fade_in"
			this.$el.find("[name='q']").first().focus()	


	show_search: (e)->
		e.preventDefault() if e?

		this.$el.addClass "fade_in"
		this.$el.find("[name='q']").first().focus()			


	hide_search: (e)->
		e.preventDefault() if e?

		this.$el.removeClass "fade_in"


